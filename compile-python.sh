#!/bin/sh
PYVER=2.7
PYCOMPILE="/usr/lib/python${PYVER}/py_compile.py"
python${PYVER} ${PYCOMPILE} `find -name '*.py'`
python${PYVER} -O ${PYCOMPILE} `find -name '*.py'`
