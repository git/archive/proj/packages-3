import cherrypy
from web.lib.query_filter import format_query

# We use short variable names!
# pylint: disable-msg=C0103

def absurl(path='', qs=None, script_name=None):
    """Better version of cherrypy.url. Generates absolute URLs."""

    if qs is None:
        qs = {}
    return cherrypy.url(
            path=path,
            qs=format_query(qs),
            script_name=script_name,
            base=None,
            relative=False)

def relurl(path='', qs=None, script_name=None):
    """Better version of cherrypy.url. Generates relative URLs."""
    if qs is None:
        qs = {}
    return cherrypy.url(
            path=path,
            qs=format_query(qs),
            script_name=script_name,
            base='',
            relative=False)

def viewcvs_link(path,
            repo = 'gentoo-x86',
            sitebase = 'sources.gentoo.org/viewcvs.py'):
    """Given a path within the specified repo, give a link to the
       ViewCVS markup page
    """

    return 'http://%s/%s/%s?view=markup' % (sitebase, repo, path)

def bugzilla_bug_link(bugid,
        sitebase='bugs.gentoo.org'):
    """Given a bug ID, give link to the relevant Bugzilla entry"""

    return 'https://%s/show_bug.cgi?id=%d' % (sitebase, bugid)

def bugzilla_search_link(searchstring,
        sitebase = 'bugs.gentoo.org',
        bug_status = None):
    """Give a quicksearch link for a Bugzilla install"""

    if bug_status is None:
        bug_status = ['UNCONFIRMED', 'IN_PROGRESS', 'CONFIRMED']

    def f(k):
        """return a bug status match"""
        return 'bug_status=%s' % (k)
    args = map(f, bug_status)
    args.append('query_format=')
    args.append('short_desc_type=allwords')
    args.append('short_desc=%s' % (searchstring, ))
    queryparams = '&'.join(args)
    s = 'https://%s/buglist.cgi?%s' % (sitebase, queryparams)
    return s

def ciavc_link(username):
    """Given a username, give link to the relevant CIA.vc entry"""
    return 'http://cia.vc/stats/author/%s' % (username)

def forums_search_link(query):
    return 'https://forums.gentoo.org/search.php?search_terms=all&show_results=topics&mode=results&search_keywords=%s' % (query)

# vim:ts=4 et ft=python:
