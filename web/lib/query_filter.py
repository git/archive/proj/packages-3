import cherrypy
from etc.const_data import ConstData

# We use short variable names!
# pylint: disable-msg=C0103

def sanitize_query_string(qsargs=None):
    """Allow ONLY the query arguments that are completely safe to pass.
    There should be NO entities."""

    newqs = {}
    if qsargs is None:
        qsargs = {}

    args_none = ['full_cat', 'local_latest']
    for arg in args_none:
        if arg in qsargs:
            newqs[arg] = ''

    args_int = ['count', 'left_daycount']
    for arg in args_int:
        if arg in qsargs:
            try:
                newqs[arg] = int(qsargs[arg])
            except ValueError:
                pass

    key = 'arches'
    if key in qsargs:
        val = qsargs[key]
        if val in ConstData.arches.keys():
            newqs[key] = val

    return newqs

def format_query(query_string=None):
    """Convert a dictionary to a query string"""

    if query_string is None:
        query_string = {}
    new_qs = []
    for key in query_string.keys():
        val = str(query_string[key])
        if val:
            val = '=' + val
        new_qs.append('%s%s' % (key, val))
    query_string = '&'.join(new_qs)
    return query_string

def check_page_variables(kwds=None):
    if kwds is None:
        kwds = {}
    # We must have this for safety
    assert 'safeqs' in kwds, \
            'safe query arguments not available!'

    if 'safeqs_arches' not in kwds:
        qsd = {}
        for i in ConstData.arches.keys():
            qsd[i] = kwds['safeqs'].copy()
            qsd[i]['arches'] = i
            # arches=common is the default
            if i is 'common':
                del qsd[i]['arches']
        kwds['safeqs_arches'] = qsd
    
    if 'cherrypy' not in kwds:
        kwds['cherrypy'] = cherrypy
    if 'self' not in kwds:
        kwds['self'] = cherrypy.request.path_info.replace('/feed','')
    if 'mainrss' not in kwds:
        kwds['mainrss'] = '/feed/'
    if 'pagerss_reltitle' not in kwds:
        (rss, title) = create_rel(cherrypy.request.path_info)
        kwds['pagerss_reltitle'] = title
        kwds['pagerss'] = rss
    return kwds

def create_rel(path):
	# This is a switch structure
	# pylint: disable-msg=R0912
    t = ''
    if path.startswith('/feed/'):
        path = path.replace('/feed','')
    
    # Strip trailing slash
    if len(path) > 1 and path[-1] == '/':
        path = path[0:-1]

    rsspath = '/feed'+path
    
    if path == '/':
        pass
    elif path.startswith('/category'):
        t = 'This category'
    elif path.startswith('/package'):
        t = 'This package'
    elif path.startswith('/arch'):
        m = path.split('/')
        if len(m) == 3:
            t = m[2]
        elif len(m) == 4:
            t = '%s %s' % (m[3], m[2])
    elif path.startswith('/verbump'):
        t = 'Version bumps'
    elif path.startswith('/newpackage'):
        t = 'New packages'
    elif path.startswith('/date'):
        t = 'New packages since a selected date'
    elif path.startswith('/faq'):
        t = 'FAQ'
        rsspath = None
    elif path.startswith('/categories'):
        t = 'Category List'
        rsspath = None
    else:
        raise Exception('Unknown page!')
    return (rsspath, t)

