# written 2007 by Robin H. Johnson <robbat2@gentoo.org>
# License: GPL-2

"""Global data"""

class ConstData(object):
    arches = {} # 'all': None, 'fbsd': None, 'linux': None, 'exotic': None, 'prefix': None, 'common': None }
    # This should match /usr/portage/profiles/arch.list
    arches['fbsd'] = frozenset([(arch+'-fbsd') for arch in ['amd64','sparc','x86']])
    arches['prefix'] = frozenset([ 'ppc-aix', 'x86-freebsd', 'x64-freebsd', 'ia64-hpux', 'hppa-hpux', 'x86-interix', 'mips-irix', 'amd64-linux', 'arm-linux', 'ia64-linux', 'x86-linux', 'ppc-macos', 'x86-macos', 'x64-macos', 'm68k-mint', 'x86-netbsd', 'ppc-openbsd', 'x86-openbsd', 'x64-openbsd', 'sparc-solaris', 'sparc64-solaris', 'x64-solaris', 'x86-solaris', 'x86-winnt', 'x86-cygwin'])
    arches['linux'] = frozenset(['alpha', 'amd64', 'arm', 'arm64', 'hppa', 'ia64', 'm68k', 'mips', 'ppc', 'ppc64', 's390', 'sh', 'sparc', 'x86'])

    arches['all'] = arches['linux'].union(arches['fbsd']).union(arches['prefix'])
    arches['exotic'] = frozenset(['arm64', 'mips', 'm68k', 's390', 'sh'])
    arches['common'] = arches['linux'].difference(arches['exotic'])

# vim:ts=4 et ft=python:
