#from pkgcore.ebuild.cpv import unversioned_CPV as CPV
import re, time
from etc.const_data import ConstData

def limit_centercount(kwds):
    centercount = 10
    if 'count' in kwds:
        try:
            centercount = int(kwds['count'])
            if centercount > 100 or centercount < 1:
                centercount = 100
        except ValueError:
            pass
    return centercount

def limit_leftcount(kwds):
    leftcount = 2
    max = 5
    if 'local_latest' in kwds:
        max = 90
        leftcount = 14
    if 'left_daycount' in kwds:
        try:
            leftcount = int(kwds['left_daycount'])
            if leftcount > max or leftcount < 1:
                leftcount = max
        except ValueError:
            pass
    return leftcount

def limit_arches(kwds):
    # Default to common
    arches = ConstData.arches['common']
    if 'arches' in kwds:
        print "Testing %s against %s" % (kwds['arches'], '/'.join(ConstData.arches.keys()))
        if kwds['arches'] in ConstData.arches.keys():
            arches =  ConstData.arches[kwds['arches']]
    return arches
        
class EntryFilters(object):
    """Filter class for latest_entries list"""

    def __init__(self, package_source):
        self.package_source = package_source
        self.valid_name = re.compile("^[a-zA-Z0-9\-\+_]*$")

    def category_filter(self, catname, use_latest=True, limit=None):
        """Skip packages not matching our category"""
        key = 'category_filter_%r%r%r' % (catname, use_latest, limit)
        def f():
            return self._category_filter(catname, use_latest, limit)
        return self.package_source.mc_wrap(key, f, time=300)

    def _category_filter(self, catname, use_latest=True, limit=None):
        """Skip packages not matching our category (uncached)"""
        if not self.valid_name.match(catname):
            return []
        pkg_list = []
        if use_latest:
            pkg_list = self.package_source.get_latest_cpvs_by_category(catname, limit)
        else:
            pkg_list = self.package_source.get_category_packages(catname)
        return pkg_list
        #for entry in pkg_list:
        #    atom = CPV(str(entry[0]))
        #    if atom.category == catname:
        #        ret_list.append(entry)
        #return ret_list

    def package_filter(self, pkgname, limit=None):
        """Skip packages not matching pkgname"""
        key = 'package_filter_%r%r' % (pkgname, limit)
        def f():
            return self._package_filter(pkgname, limit)
        return self.package_source.mc_wrap(key, f, time=300)

    def _package_filter(self, pkgname, limit=None):
        """Skip packages not matching pkgname (uncached)"""
        if not self.valid_name.match(pkgname):
            return []
        return self.package_source.get_latest_cpvs_by_pkgname(pkgname, limit)

    def category_package_filter(self, category, pkgname, limit=None):
        """Skip packages not matching pkgname and category"""
        key = 'category_package_filter_%r%r%r' % (category, pkgname, limit)
        def f():
            return self._category_package_filter(category, pkgname, limit)
        return self.package_source.mc_wrap(key, f, time=300)

    def _category_package_filter(self, category, pkgname, limit=None):
        """Skip packages not matching pkgname and category (uncached)"""
        if not self.valid_name.match(pkgname):
            return []
        return self.package_source.get_latest_cpvs_by_category_pkgname(category, pkgname, limit)

    def hardmasked_filter(self, arch, limit=None):
        """Filter packages by hardmasked for arch"""
        key = 'hardmasked_filter_%r%r' % (arch, limit)
        def f():
            return self._hardmasked_filter(arch, limit)
        return self.package_source.mc_wrap(key, f, time=300)

    def _hardmasked_filter(self, arch, limit=None):
        """Filter packages by hardmasked for arch (uncached)"""
        return self.package_source.get_latest_cpvs_by_arch(arch, 'M', limit)

    def stable_filter(self, arch, limit=None):
        """Filter packages by stable keyword for arch"""
        key = 'stable_filter_%r%r' % (arch, limit)
        def f():
            return self._stable_filter(arch, limit)
        return self.package_source.mc_wrap(key, f, time=300)

    def _stable_filter(self, arch, limit=None):
        """Filter packages by stable keyword for arch (uncached)"""
        return self.package_source.get_latest_cpvs_by_arch(arch, '+', limit)

    def unstable_filter(self, arch, limit=None):
        """Filter packages by unstable keyword for arch"""
        key = 'unstable_filter_%r%r' % (arch, limit)
        def f():
            return self._unstable_filter(arch, limit)
        return self.package_source.mc_wrap(key, f, time=300)

    def _unstable_filter(self, arch, limit=None):
        """Filter packages by unstable keyword for arch (uncached)"""
        return self.package_source.get_latest_cpvs_by_arch(arch, '~', limit)

    def arch_filter(self, arch, limit=None):
        """Filter packages by any keyword for arch"""
        key = 'arch_filter_%r%r' % (arch, limit)
        def f():
            return self._arch_filter(arch, limit)
        return self.package_source.mc_wrap(key, f, time=300)

    def _arch_filter(self, arch, limit=None):
        """Filter packages by any keyword for arch (uncached)"""
        return self.package_source.get_latest_cpvs_by_arch(arch, '', limit)

    def verbumps_filter(self, limit=None):
        """Filter packages to version bumps only"""
        key = 'verbumps_filter_%r' % (limit, )
        def f():
            return self._verbumps_filter(limit)
        return self.package_source.mc_wrap(key, f, time=300)

    def _verbumps_filter(self, limit=None):
        """Filter packages to version bumps only (uncached)"""
        return self.package_source.get_latest_cpvs_by_verbump(limit)

    def newpkgs_filter(self, limit=None):
        """Filter packages to new packages only"""
        key = 'newpkgs_filter_%r' % (limit, )
        def f():
            return self._newpkgs_filter(limit)
        return self.package_source.mc_wrap(key, f, time=300)

    def _newpkgs_filter(self, limit=None):
        """Filter packages to new packages only (uncached)"""
        return self.package_source.get_latest_cpvs_by_newpkg(limit)

    def date_filter(self, date, limit=None):
        """Filter packages to new by date only"""
        key = 'date_filter_%s_%r' % (time.strftime("%Y%m%d",date), limit, )
        def f():
            return self._date_filter(date, limit)
        return self.package_source.mc_wrap(key, f, time=300)

    def _date_filter(self, date, limit=None):
        """Filter packages to new packages only since date (uncached)"""
        return self.package_source.get_latest_cpvs_by_date(date, limit)

    def unfiltered(self, limit=None):
        """unfiltered list"""
        key = 'unfiltered_%r' % (limit, )
        def f():
            return self._unfiltered(limit)
        return self.package_source.mc_wrap(key, f, time=300)

    def _unfiltered(self, limit=None):
        """unfiltered list"""
        return self.package_source.get_latest_cpvs(limit)

    def latest_entry(self):
        """return only latest entry"""
        ret_val = self.unfiltered(1)
        if ret_val:
            return ret_val[0]
        return (0, 0)

# vim:ts=4 et ft=python:
