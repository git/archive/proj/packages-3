#!/bin/sh
#CODEDIR=/var/www/packages.gentoo.org
CODEDIR=$(dirname $0)
cd ${CODEDIR}
source ${CODEDIR}/etc/make.conf
if test -z "${SYNC}" || test -z "${PORTDIR}"; then
        echo "Cannot find SYNC or PORTDIR!" 1>&2
        exit 1
fi
rsync ${RSYNC_OPTS} ${SYNC} ${PORTDIR}
rc=$?
if [ $rc -ne 0 ]; then
	echo "Failed to rsync!" 1>&2
	exit 1
fi
CONFIG_ROOT=. PORTAGE_CONFIGROOT=. PYTHONPATH=. python dbgenerator/core.py
pkill -HUP -u gpackages -f 'web/controller.py'
