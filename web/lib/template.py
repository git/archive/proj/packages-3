# ripped from geddit demo

import os

import cherrypy
import time
from genshi.core import Stream
from genshi.output import encode, get_serializer
from genshi.template import Context, TemplateLoader
from web.lib import ajax
from web.lib.query_filter import check_page_variables
from web.lib.headers import httpheader_expires

loader = TemplateLoader(
    os.path.join(os.path.dirname(__file__), '..', 'templates'),
    auto_reload=True
)

def output(filename, method='html', encoding='utf-8', **options):
    """Decorator for exposed methods to specify what template the should use
    for rendering, and which serialization method and options should be
    applied.
    """
    def decorate(func):
        def wrapper(*args, **kwargs):
            cherrypy.thread_data.template = loader.load(filename)
            opt = options.copy()
            if not ajax.is_xhr() and method == 'xhtml':
                opt.setdefault('doctype', 'xhtml')
            serializer = get_serializer(method, **opt)
            stream = func(*args, **kwargs)
            if not isinstance(stream, Stream):
                return stream
            return encode(serializer(stream), method=serializer,
                          encoding=encoding)
        return wrapper
    return decorate


def render(*args, **kwargs):
    """Function to render the given data to the template specified via the
    ``@output`` decorator.
    """
    if args:
        assert len(args) == 1, \
            'Expected exactly one argument, but got %r' % (args,)
        template = loader.load(args[0])
    else:
        template = cherrypy.thread_data.template

    kwargs = check_page_variables(kwargs)

    ctxt = Context(url=cherrypy.url)
    ctxt.push(kwargs)
    text = template.generate(ctxt)
    return text

def expire_on_30_min():
    """Expire the page on the next 30 minute interval
    """
    def decorate(func):
        def newfunc(*args, **kwargs):
            cherrypy.response.headers['Expires'] = httpheader_expires()
            cherrypy.response.headers['Cache-Control'] = 'max-age=1800, public, s-maxage=1800'
            return func(*args,**kwargs)
        return newfunc
    return decorate

# vim:ts=4 et ft=python:
