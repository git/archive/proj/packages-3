from time import strftime, gmtime, time, localtime

def _lastmodified_strftime(format, latest_entry):
    """return a formatted timestamp of latest entry
        with the specified strftime"""
    return strftime(format, localtime(latest_entry[1]))

def lastmodified_rightcontent(latest_entry):
    """return a formatted timestamp of latest entry (being last change)"""
    fmt = '%d %b %Y<br />%H:%M %Z'
    return _lastmodified_strftime(fmt, latest_entry)

def lastmodified_httpheader(latest_entry):
    """return a HTTP Last-Modified value of the latest entry"""
    fmt = '%a, %d %b %Y %H:%M:%S GMT'
    return _lastmodified_strftime(fmt, latest_entry)

def lastmodified_rss(latest_entry):
    """return a RSS time string of the latest entry"""
    fmt = '%Y-%m-%dT%H:%M:%SZ'
    return _lastmodified_strftime(fmt, latest_entry)

def pageexpire_timestamp():
    expire_ts = int(time())
    # Work out when the LAST 30 minute window was
    expire_ts = expire_ts - expire_ts % (30*60)
    # Now find the next one
    expire_ts += (30*60)
    # We run at every 15,45
    # And take a worst case of 2 minutes
    expire_ts += (17*60)
    return expire_ts

def httpheader_expires():
    expire_ts = pageexpire_timestamp()
    return strftime('%a, %d %b %Y %H:%M:%S GMT', gmtime(expire_ts))

# vim:ts=4 et ft=python:
